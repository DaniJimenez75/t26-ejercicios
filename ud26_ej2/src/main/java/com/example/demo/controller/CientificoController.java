package com.example.demo.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Cientifico;
import com.example.demo.service.CientificoServiceImpl;
import com.example.demo.service.ProyectoServiceImpl;


@RestController
@RequestMapping("/api")
public class CientificoController {

	@Autowired
	CientificoServiceImpl cientificoService;
	
	@GetMapping("/cientifico")
	public List<Cientifico> listarCientifico(){
		return cientificoService.listarCientifico();
	}
	
	
	@PostMapping("/cientifico")
	public Cientifico salvarCientifico(@RequestBody Cientifico cientifico) {
		
		return cientificoService.guardarCientifico(cientifico);
	}
	
	
	@GetMapping("/cientifico/{dni}")
	public Cientifico CientificoXDNI(@PathVariable(name="dni") String dni) {
		
		Cientifico cientifico_xid= new Cientifico();
		
		cientifico_xid=cientificoService.CientificoXDNI(dni);
		
		System.out.println("Cientifico XID: "+cientifico_xid);
		
		return cientifico_xid;
	}
	
	@PutMapping("/cientifico/{dni}")
	public Cientifico actualizarCientifico(@PathVariable(name="dni")String dni,@RequestBody Cientifico cientifico) {
		
		Cientifico cientifico_seleccionado= new Cientifico();
		Cientifico cientifico_actualizado= new Cientifico();
		
		cientifico_seleccionado= cientificoService.CientificoXDNI(dni);
		
		cientifico_seleccionado.setNomapels(cientifico.getNomapels());
		
		cientifico_actualizado = cientificoService.actualizarCientifico(cientifico_actualizado);
		
		System.out.println("El cientifico actualizada es: "+ cientifico_actualizado);
		
		return cientifico_actualizado;
	}
	
	@DeleteMapping("/cientifico/{dni}")
	public void eliminarCientifico(@PathVariable(name="dni")String dni) {
		cientificoService.eliminarCientifico(dni);
	}
}
