package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IProductosDAO;
import com.example.demo.dto.Productos;

@Service
public class ProductoServiceImpl implements IProductoService{

	@Autowired
	IProductosDAO iProductosDAO;
	
	@Override
	public List<Productos> listarProductos() {
		return iProductosDAO.findAll();
	}

	@Override
	public Productos guardarProductos(Productos productos) {
		return iProductosDAO.save(productos);
	}

	@Override
	public Productos productosXCodigo(int codigo) {
		return iProductosDAO.findById(codigo).get();
	}

	@Override
	public Productos actualizarProductos(Productos productos) {
		return iProductosDAO.save(productos);
	}

	@Override
	public void eliminarProductos(int codigo) {
		iProductosDAO.deleteById(codigo);
		
	}

}
