package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.MaquinaRegistradora;



public interface IMaquinaRegistradoraService {

	
	//Metodos del CRUD
			public List<MaquinaRegistradora> listarMaquinaRegistradora(); //Listar All 
			
			public MaquinaRegistradora guardarMaquinaRegistradora(MaquinaRegistradora maquinaRegistradora);	//Guarda un Curso CREATE
			
			public MaquinaRegistradora MaquinaRegistradoraXID(int codigo); //Leer datos de un Curso READ
			
			public MaquinaRegistradora actualizarMaquinaRegistradora(MaquinaRegistradora maquinaRegistradora); //Actualiza datos del Curso UPDATE
			
			public void eliminarMaquinaRegistradora(int codigo);// Elimina el Curso DELETE
}
