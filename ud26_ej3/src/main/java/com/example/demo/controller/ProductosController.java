package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Productos;
import com.example.demo.service.CajeroServiceImpl;
import com.example.demo.service.ProductoServiceImpl;


@RestController
@RequestMapping("/api")
public class ProductosController {

	@Autowired
	ProductoServiceImpl productoService;
	
	@GetMapping("/producto")
	public List<Productos> listarProducto(){
		return productoService.listarProductos();
	}
	
	
	@PostMapping("/producto")
	public Productos salvarProducto(@RequestBody Productos producto) {
		
		return productoService.guardarProductos(producto);
	}
	
	
	@GetMapping("/producto/{codigo}")
	public Productos productoXCodigo(@PathVariable(name="codigo") int codigo) {
		
		Productos producto_xCodigo= new Productos();
		
		producto_xCodigo=productoService.productosXCodigo(codigo);
		
		System.out.println("Codigo XCodigo: "+producto_xCodigo);
		
		return producto_xCodigo;
	}
	
	@PutMapping("/producto/{codigo}")
	public Productos actualizarProducto(@PathVariable(name="codigo")int codigo,@RequestBody Productos producto) {
		
		Productos producto_seleccionado= new Productos();
		Productos producto_actualizado= new Productos();
		
		producto_seleccionado= productoService.productosXCodigo(codigo);
		
		producto_seleccionado.setNombre(producto.getNombre());
		producto_seleccionado.setPrecio(producto.getPrecio());
		
		producto_actualizado = productoService.actualizarProductos(producto);
		
		System.out.println("El producto actualizado es: "+ producto_actualizado);
		
		return producto_actualizado;
	}
	
	@DeleteMapping("/producto/{codigo}")
	public void eleiminarCurso(@PathVariable(name="codigo")int codigo) {
		productoService.eliminarProductos(codigo);
	}
	
}
