package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

@Entity
@Table(name="cajeros")
public class Cajero {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="codigo")
	private int codigo;
	@Column(name="nomapels")
	private String nomApels;

	@OneToMany
	@Column(name="id")
	private List<Venta> cajero;

	/**
	 * 
	 */
	public Cajero() {
	}

	/**
	 * @param id
	 * @param nombre
	 * @param proveedores
	 */
	public Cajero(int codigo, String nomApels, List<Venta> cajero) {
		super();
		this.codigo = codigo;
		this.nomApels = nomApels;
		this.cajero = cajero;
	}

	

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNomApels() {
		return nomApels;
	}

	public void setNomApels(String nomApels) {
		this.nomApels = nomApels;
	}

	public List<Venta> getCajero() {
		return cajero;
	}
	
	/**
	 * @param proveedores the proveedores to set
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Venta")
	public void setCajero(List<Venta> cajero) {
		this.cajero = cajero;
	}

	@Override
	public String toString() {
		return "Cajero [codigo=" + codigo + ", nomApels=" + nomApels + "]";
	}

	
	
	
	
		
}
