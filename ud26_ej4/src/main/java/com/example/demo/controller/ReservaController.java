package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Investigador;
import com.example.demo.dto.Reserva;
import com.example.demo.service.ReservaServiceImpl;

@RestController
@RequestMapping("/api")
public class ReservaController {
	
	@Autowired
	ReservaServiceImpl reservaService;
	
	@GetMapping("/reservas")
	public List<Reserva> listarReservas(){
		return reservaService.listarReservas();
	}
	
	@PostMapping("/reservas")
	public Reserva salvarReserva(@RequestBody Reserva reserva) {
		return reservaService.guardarReserva(reserva);
	}
	
	@GetMapping("/reservas/{id}")
	public Reserva reservaXID(@PathVariable(name="id") int id) {
		
		Reserva reserva_xid= new Reserva();
		
		reserva_xid=reservaService.reservaXID(id);
		
		System.out.println("Reserva XID: "+reserva_xid);
		
		return reserva_xid;
	}
	
	@PutMapping("/reservas/{id}")
	public Reserva actualizarReserva(@PathVariable(name="id")int id,@RequestBody Reserva reserva) {
		
		Reserva reserva_seleccionado= new Reserva();
		Reserva reserva_actualizado= new Reserva();
		
		reserva_seleccionado= reservaService.reservaXID(id);
		
		reserva_seleccionado.setInvestigador(reserva.getInvestigador());
		
		reserva_seleccionado.setEquipo(reserva.getEquipo());
		
		reserva_seleccionado.setInicio(reserva.getInicio());
		
		reserva_seleccionado.setFin(reserva.getFin());
		
		reserva_actualizado = reservaService.actualizarReserva(reserva);
		
		System.out.println("La reserva actualizada es: "+ reserva_actualizado);
		
		return reserva_actualizado;
	}
	
	@DeleteMapping("/reservas/{id}")
	public void eleiminarReserva(@PathVariable(name="id")int id) {
		reservaService.eliminarReserva(id);;
	}
}
