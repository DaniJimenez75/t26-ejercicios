package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IPiezaProveedorDAO;
import com.example.demo.dto.PiezaProveedor;


@Service
public class PiezaProveedorServiceImpl implements IPiezaProveedorService {

	@Autowired
	IPiezaProveedorDAO piezaProveedorDao;
	
	@Override
	public List<PiezaProveedor> listarPiezaProveedor() {
		return piezaProveedorDao.findAll();
	}

	@Override
	public PiezaProveedor guardarPiezaProveedor(PiezaProveedor piezaProveedor) {
		return piezaProveedorDao.save(piezaProveedor);
	}

	@Override
	public PiezaProveedor piezaProveedorXID(int id) {
		return piezaProveedorDao.findById(id).get();
	}

	@Override
	public PiezaProveedor actualizarPiezaProveedor(PiezaProveedor piezaProveedor) {
		return piezaProveedorDao.save(piezaProveedor);
	}

	@Override
	public void eliminarPiezaProveedor(int id) {
		piezaProveedorDao.deleteById(id);
		
	}

}
